<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Admin;
use Auth;
use URL;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
//Enables us to output flash messaging
use Session;
use Image;

class AdminController extends Controller {

    public function __construct() {
        //$this->middleware(['auth', 'isAdmin']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //Get all users and pass it to the view
        return view('admin.users.index');
    }

    /**
     * To display dynamic table by datatable
     *
     * @return mixed
     */
    public function datatable() {
        return Datatables::of(Admin::select('*')->where('id', '!=', 1)->where('type', '=', 'admin'))
                        ->rawColumns(['actions'])
                        ->editColumn('created_at', function ($user) {
                            return $user->created_at->format('F d, Y h:ia');
                        })->editColumn('roles', function ($user) {
                    return $user->roles()->pluck('name')->implode(' ');
                })->editColumn('actions', function ($user) {
                    $currentUser = Auth::guard('admin')->user();

                    // Admin can edit and delete anyone...
                    if ($currentUser->hasRole('super-admin', 'admin')) {
                        $b = '<a href="' . URL::route('users.edit', $user->id) . '" class="btn btn-primary btn-xs mrs"><i class="fa fa-pencil"></i></a>';

                        // ...except delete himself
                        if ($user->id !== $currentUser->id) {
                            $b .= '<a href="' . URL::route('users.destroy', $user->id) . '" class="btn btn-danger btn-xs destroy"><i class="fa fa-trash"></i></a>';
                        }
                        return $b;
                    }

                    // The user is the current user, you can't delete yourself
                    if ($user->id === $currentUser->id && $currentUser->hasPermissionTo('user_update', 'admin')) {
                        return '<a href="' . URL::route('users.edit', $user) . '" class="btn btn-primary btn-xs mrs"><i class="fa fa-pencil"></i></a>';
                    }

                    if ($currentUser->hasPermissionTo('user_update', 'admin')) {
                        $b = '<a href="' . URL::route('users.edit', $user->id) . '" class="btn btn-primary btn-xs mrs"><i class="fa fa-pencil"></i></a>';
                    }

                    if ($currentUser->hasPermissionTo('user_delete', 'admin')) {
                        $b .= '<a href="' . URL::route('users.destroy', $user->id) . '" class="btn btn-danger btn-xs destroy"><i class="fa fa-trash"></i></a>';
                    }

                    return $b;
                })->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //Get all roles and pass it to the view
        $roles = Role::where('id', '!=', 1)->where('guard_name', '=', 'admin')->get();
        return view('admin.users.create', ['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //Validate name, email and password fields
        $this->validate($request, [
            'name' => 'required|max:120',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);

        $request->merge(['type' => 'admin', 'admin_id' => Auth::guard('admin')->user()->id]);
        $user = Admin::create($request->only('email', 'name', 'password', 'type'));

        $roles = $request['roles']; //Retrieving the roles field
        //Checking if a role was selected
        if (isset($roles)) {

            foreach ($roles as $role) {
                $role_r = Role::where('id', '=', $role)->firstOrFail();
                $user->assignRole($role_r); //Assigning role to user
            }
        }
        //Redirect to the users.index view and display message
        return redirect()->route('users.index')
                        ->with('growl', ['User successfully added.', 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return redirect('users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $user = Admin::findOrFail($id); //Get user with specified id
        $roles = Role::where('id', '!=', 1)->where('guard_name', '=', 'admin')->get(); //Get all roles

        return view('admin.users.edit', compact('user', 'roles')); //pass user and roles data to view
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $user = Admin::findOrFail($id); //Get role specified by id
        //Validate name, email and password fields  
        $this->validate($request, [
            'name' => 'required|max:120',
            'email' => 'required|email|unique:users,email,' . $id,
            'password_confirmation' => 'same:password'
        ]);
        $input = $request->only(['name', 'email', 'password']); //Retreive the name, email and password fields
        $roles = $request['roles']; //Retreive all roles

        if ($input['password'] !== null) {
            $input['remember_token'] = str_random(32);
        } else {
            unset($input['password']);
        }

        $user->fill($input)->save();

        if (isset($roles)) {
            $user->roles()->sync($roles);  //If one or more role is selected associate user to roles          
        } else {
            $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
        }
        return redirect()->route('users.index')
                        ->with('growl', ['User successfully edited.', 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Admin::destroy($id);
    }

    public function profile() {
        return view('admin.users.profile', ['user' => Auth::guard('admin')->user()]);
    }

    public function profilePost(Request $request) {
        $user = Auth::guard('admin')->user();

        $this->validate($request, [
            'avatar' => 'mimes:jpeg,png|max:10000',
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'password_confirmation' => 'same:password'
        ]);

        $avatar = $request->file('avatar');

        if ($avatar && $file = $avatar->isValid()) {
            $destinationPath = dirname($user->avatar_path);
            if (!is_dir($destinationPath))
                mkdir($destinationPath, 0766, true);
            $extension = $avatar->getClientOriginalExtension();
            $fileName = md5($user->id . $user->created_at) . '_tmp.' . $extension;
            $avatar->move($destinationPath, $fileName);

            Image::make($destinationPath . DIRECTORY_SEPARATOR . $fileName)
                    ->fit(100, 100)
                    ->save($user->avatar_path);

            unlink($destinationPath . DIRECTORY_SEPARATOR . $fileName);
        }

        $input = $request->all();

        if ($input['password'] !== null) {
            //$input[ 'password' ] = $input[ 'password' ];
            $input['remember_token'] = str_random(32);
        } else {
            unset($input['password']);
        }

        $user->update($input);

        return redirect()->route('user.profile')->with('growl', [trans('myadmin.profile.successupdate'), 'success']);
    }

    public function avatarDelete() {
        $user = Auth::guard('admin')->user();
        if (is_file($user->avatar_path)) {
            unlink($user->avatar_path);
        }
    }

}
