<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Session;

class PermissionController extends Controller {

    public function __construct() {
        //$this->middleware(['auth', 'isAdmin']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $permissions = Permission::where('guard_name', '=', 'admin')->get(); //Get all permissions

        return view('admin.permissions.index')->with('permissions', $permissions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $roles = Role::where('id','!=',1)->where('guard_name', '=', 'admin')->get(); //Get all roles

        return view('admin.permissions.create')->with('roles', $roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:40',
        ]);

        $name = $request['name'];

        $permissions = [$name . '_create', $name . '_read', $name . '_update', $name . '_delete'];
        foreach ($permissions as $k => $v) {
            $permission = new Permission();
            $permission->name = $v;
            $permission->guard_name = 'admin';
            $permission->save();
        }

        $roles = $request['roles'];

        if (!empty($request['roles'])) { //If one or more role is selected
            foreach ($roles as $role) {
                $r = Role::where('id', '=', $role)->firstOrFail(); //Match input role to db record

                foreach ($permissions as $k => $v) {
                    $permission = Permission::where('name', '=', $v)->first(); //Match input //permission to db record
                    $r->givePermissionTo($permission);
                }
            }
        }

        return redirect()->route('permissions.index')
                        ->with('growl', ['Permission ' . $name . ' added!', 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return redirect('admin/permissions');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $permission = Permission::findOrFail($id);

        return view('admin.permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $permission = Permission::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|max:40',
        ]);
        $input = $request->all();
        $permission->fill($input)->save();

        return redirect()->route('permissions.index')
                        ->with('growl', ['Permission ' . $permission->name . ' updated!', 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $permission = Permission::findOrFail($id);

        //Make it impossible to delete this specific permission 
        /*if ($permission->name == "Administer roles & permissions") {
            return redirect()->route('permissions.index')
                            ->with('growl', ['Cannot delete this Permission!', 'danger']);
        }*/

        $permission->delete();

        return redirect()->route('permissions.index')
                        ->with('growl', ['Permission deleted!', 'success']);
    }

}
