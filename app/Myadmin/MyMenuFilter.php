<?php

namespace App\Myadmin;

use JeroenNoten\LaravelAdminLte\Menu\Builder;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;
use Spatie;
use Auth;

class MyMenuFilter implements FilterInterface {

    public function transform($item, Builder $builder) {
        
        $user = Auth::guard('admin')->user();
        if (isset($item['can']) && !$user->hasPermissionTo($item['can'],'admin') && !$user->hasRole('super-admin','admin')) {
            return false;
        }

        return $item;
    }

}
