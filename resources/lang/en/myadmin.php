<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'confirm-delete' => 'Are you sure want to delete this?.',
    'profile' => [
        'title'         => 'User profile',
        'subscribedsince' => 'Member since :date ( :since )',
        'avatar'        => 'Profile image',
        'delavatar'     => 'Remove the profile image',
        'confirmdelavatar' => 'Remove the profile image ?',
        'successdelavatar' => 'Profile image has been removed',
        'successupdate' => 'The profile has been correctly updated'
    ]

];
