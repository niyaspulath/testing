@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border"></div>
            <div class="box-body">
                <p>Welcome {{Auth::guard('admin')->user()->name}}</p>
            </div>
        </div>
    </div>
</div>    
@stop