@extends('adminlte::page')

@section('title', 'Permissions')

@section('content_header')
<h1><i class="fa fa-key"></i>Available Permissions
    @stop

    @section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header">
                    <?php $user = Auth::guard('admin')->user(); ?>
                    @if($user->hasRole('super-admin','admin') || $user->hasPermissionTo('user_read','admin'))
                    <a href="{{ route('users.index') }}" class="btn btn-default pull-right btn-sm">Users</a>
                    @endif
                    @if($user->hasRole('super-admin','admin') || $user->hasPermissionTo('role_read','admin'))
                    <a href="{{ route('roles.index') }}" class="btn btn-default pull-right btn-sm">Roles</a>
                    @endif
                    @if($user->hasRole('super-admin','admin') || $user->hasPermissionTo('permission_create','admin'))
                    <a href="{{ URL::to('admin/permissions/create') }}" class="btn btn-success pull-right btn-sm">Add Permission</a>
                    @endif
                </div>
                <div class="box-body">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="perm-table">

                            <thead>
                                <tr>
                                    <th>Permissions</th>
                                    <th>Operation</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($permissions as $permission)
                                <tr>
                                    <td>{{ $permission->name }}</td> 
                                    <td>
                                        @if($user->hasRole('super-admin','admin') || $user->hasPermissionTo('permission_update','admin'))
                                        <a href="{{ URL::to('admin/permissions/'.$permission->id.'/edit') }}" class="btn btn-xs btn-info pull-left" style="margin-right: 3px;">Edit</a>
                                        @endif

                                        @if($user->hasRole('super-admin','admin') || $user->hasPermissionTo('permission_delete','admin'))
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id] ]) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger destroy']) !!}
                                        {!! Form::close() !!}
                                        @endif

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @stop
    @section('js')
    <script type='text/javascript'>
        $(function () {
            $('#perm-table').on('click', '.destroy', function (e) {
                e.preventDefault();
                var form = $(this);
                bootbox.confirm("{{ trans('myadmin.confirm-delete') }}", function (result) {
                    if (result === false) {
                        return;
                    } else {
                        form.parent().submit();
                    }
                });
            });
        });
    </script>    
    @stop