@extends('adminlte::page')

@section('title', 'Edit Role')

@section('content_header')
<h1><i class='fa fa-key'></i> Edit Role: {{$role->name}}</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="box box-danger">
            <div class="x_panel">
                <div class="x_content">

                    {{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT')) }}
                    <div class="box-body">
                         <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            {{ Form::label('name', 'Role Name') }}
                            {{ Form::text('name', null, array('class' => 'form-control')) }}
                             {!! $errors->first('name','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>

                        <div class="panel panel-default ">

                            <div class="panel-heading"><h5><b>Assign Permissions</b></h5></div>

                            <div class="panel-body">

                                @foreach ($permissions as $k => $v)
                                @if(is_array($v))
                                <div class="panel panel-default" style="clear: both;">
                                    <div class="panel-body">
                                        <div class="panel-heading"><b>{{ucfirst($k)}}</b></div>
                                        @foreach ($v as $k1 => $v1)
                                        <div class="col-md-3">
                                            {{ Form::checkbox('permissions[]',  $k1,  $role->permissions, ['class'=>'checkboxRole minimal-red'] ) }}
                                            {{ Form::label($v1, ucfirst($v1)) }}
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @else
                                <div class="col-md-3">
                                    {{ Form::checkbox('permissions[]',  $v,  $role->permissions, ['class'=>'checkboxRole minimal-red'] ) }}
                                    {{ Form::label($k, ucfirst($k)) }}
                                </div>
                                @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }}    
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('css')
<link rel="stylesheet" href="{{asset('vendor/adminlte/plugins/iCheck/all.css')}}">
@stop
@section('js')
<script src="{{asset('vendor/adminlte/plugins/iCheck/icheck.min.js')}}"></script>
<script type='text/javascript'>
$(function () {
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass: 'iradio_minimal-red'
    })
});
</script>    
@stop