@extends('adminlte::page')

@section('title', 'My Profile')

@section('content_header')
<h1><i class='fa fa-user'></i> My Profile</h1>
@stop

@section('content')
    {{ Form::open(['route' => ['user.profile'], 'method' => 'post', 'autocomplete' => 'off', 'files' => true]) }}
        <div class="row">
            <div class="col-sm-12 mbl">
                <span class="btn-group pull-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                </span>
            </div>
        </div>
    <br style="clear: both;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                <div class="info-box">
                    <span class="info-box-icon" style="line-height: normal">
                        <img src="{{ $user->avatar_url }}" class="avatar" alt="avatar"/>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">
                            <p class="mbn"><strong class="h3">{{ $user->name  }}</strong></p>
                            <p class="">{{ $user->roles()->pluck('name')->implode(' ') }}</p>
                        </span>
                        <span class="info-box-more">
                            <p class="mbn text-muted">
                                {{ trans('myadmin.profile.subscribedsince', [
                                    'date' => $user->created_at->format(trans('date.lFdY')),
                                    'since' => $user->created_at->diffForHumans()]) }}
                            </p>
                        </span>
                    </div>
                </div>
                <div class="box box-info">
                    <div class="box-header">
                        @if(is_file($user->avatar_path))
                        <span class="pull-right">
                            <button class="btn btn-xs btn-default" id="remove_avatar">
                                {{ trans('myadmin.profile.delavatar') }}
                            </button>
                        </span>
                        @endif
                        <h3 class="box-title">{{ trans('myadmin.profile.avatar') }}</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
                            {!! Form::file('avatar', ['id' => 'avatar']) !!}
                            {!! $errors->first('avatar','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Informations</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                    {{ Form::label('name', 'Name') }}
                                    {{ Form::text('name', old('name', $user->name), ['class' => 'form-control', 'autofocus']) }}
                                    {!! $errors->first('name','<p class="text-danger"><strong>:message</strong></p>') !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                    {{ Form::label('email', 'Email') }}
                                    {{ Form::text('email', old('email', $user->email), ['class' => 'form-control']) }}
                                    {!! $errors->first('email','<p class="text-danger"><strong>:message</strong></p>') !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                    {{ Form::label('password', 'Password') }}
                                    {{ Form::password('password', ['class' => 'form-control']) }}
                                    {!! $errors->first('password','<p class="text-danger"><strong>:message</strong></p>') !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                                    {{ Form::label('password_confirmation', 'Confirm Password') }}
                                    {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
                                    {!! $errors->first('password_confirmation','<p class="text-danger"><strong>:message</strong></p>') !!}
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {{ Form::close() }}
@stop

@section('css')
<link rel="stylesheet" href="{!! asset('vendor/adminlte/plugins/bootstrap-fileinput/css/fileinput.min.css') !!}">
@stop

@section('js')
<script src="{!! asset('vendor/adminlte/plugins/bootstrap-fileinput/js/fileinput.min.js') !!}"></script>
<script>
    $('#avatar').fileinput({
        showUpload: false,
        uploadAsync: false
    });

    $('#remove_avatar').on('click', function(e){
        e.preventDefault();

        bootbox.confirm("{{ trans('myadmin.profile.confirmdelavatar') }}", function(e){
            if(e === false) return;

            $.ajax({
                url: '{{ route('user.avatardelete') }}',
                type: 'post',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                cache: false,
                success: function(res) {
                    $('.avatar').attr('src', "{{ asset('/images/default_user.png') }}");
                    growl("{{ trans('myadmin.profile.successdelavatar') }}", "success");
                    $('#remove_avatar').remove();
                }
            });
        })
    });

</script>
@stop